﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class ColaCliente : Queue<Solicitud>
    {
        public ColaCliente() { }

        // Método que agrega una solicitud a la cola
        public void AgregarSolicitud(string nombreCliente, string tipoSolicitud)
        {
            Solicitud solicitud = new Solicitud(nombreCliente, tipoSolicitud);
            this.Enqueue(solicitud);
        }

        // Método que muestra por pantalla la siguiente solicitud de atención en la cola
        public void MostrarSiguienteSolicitud()
        {
            Solicitud siguienteSolicitud = this.Peek();
            Console.WriteLine("Siguiente solicitud: {0} - {1}", siguienteSolicitud.NombreCliente, siguienteSolicitud.TipoSolicitud);
        }

        // Método que elimina la solicitud de atención que está en la cabeza de la cola
        public void EliminarSolicitudAtendida()
        {
            this.Dequeue();
            Console.WriteLine("Solicitud eliminada de la cola");
        }

        // Método que muestra por pantalla la cantidad de solicitudes de atención pendientes en la cola
        public void SolicitudesPendientes()
        {
            Console.WriteLine("Solicitudes pendientes: {0}", this.Count);
        }
    }
}
