﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class Solicitud
    {
        public string NombreCliente;
        public string TipoSolicitud;

        // Constructor de la clase de Solicitud
        public Solicitud(string nombreCliente, string tipoSolicitud)
        {
            NombreCliente = nombreCliente;
            TipoSolicitud = tipoSolicitud;
        }
        public override string ToString()
        {
            return "Nombre del cliente: " + NombreCliente + " ,Tipo de solicitud: " + TipoSolicitud;
        }
    }
}
