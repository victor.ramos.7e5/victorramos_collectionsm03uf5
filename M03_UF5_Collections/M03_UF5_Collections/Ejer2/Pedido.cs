﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class Pedido
    {
        public string NombreCliente { get; }
        public string DireccionEntrega { get; }
        public List<string> Platos { get; }

        //Constructor de la classe Pedido
        public Pedido(string nombreCliente, string direccionEntrega, List<string> platos)
        {
            NombreCliente = nombreCliente;
            DireccionEntrega = direccionEntrega;
            Platos = platos;
        }

        public override string ToString()
        {
            string platosString = string.Join(", ", Platos);
            return $"Cliente: {NombreCliente}, Dirección de entrega: {DireccionEntrega}, Platos: {platosString}";
        }
    }
}
