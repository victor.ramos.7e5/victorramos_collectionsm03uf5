﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class ColaReparto : Queue<Pedido>
    {
        public void AgregarPedido(string nombreCliente, string direccionEntrega, List<string> platos)
        {
            Pedido nuevoPedido = new Pedido(nombreCliente, direccionEntrega, platos);
            this.Enqueue(nuevoPedido);
        }
        // Permite mostrar el porximo pedido que contiene la cola
        public void MostrarProximoPedido()
        {
            Pedido proximoPedido = this.Peek();
            Console.WriteLine("Próximo pedido en la cola:");
            Console.WriteLine(proximoPedido.ToString());
        }

        // Permite eliminar el porximo pedido que contiene la cola
        public void EliminarProximoPedido()
        {
            Pedido pedidoEntregado = this.Dequeue();
            Console.WriteLine("Pedido entregado:");
            Console.WriteLine(pedidoEntregado.ToString());
        }
    }
}
