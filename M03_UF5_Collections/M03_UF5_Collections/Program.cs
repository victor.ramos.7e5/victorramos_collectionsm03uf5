﻿using System;
using System.Collections.Generic;

namespace M03_UF5_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Inici();

        }

        // Inici del programa on s'executa el menu
        void Inici()
        {
            bool fin;
            do
            {
                Console.Clear();
                MostrarMenu();
                string opcion = Console.ReadLine();
                fin = CallExercice(opcion);
            } while (!fin);

        }

        // Es mostra els exercicis que es poden fer
        void MostrarMenu()
        {
            Console.WriteLine("Selecciona l'exercici");
            Console.WriteLine("[1]: Exercici 1");
            Console.WriteLine("[2]: Exercici 2");
            Console.WriteLine("[3]: Exercici 3");
            Console.WriteLine("[0]: Sortir");
        }

        //Permet seleccionar l'exercici per jugar
        bool CallExercice(string opcion)
        {
            Console.Clear();
            switch (opcion)
            {
                case "1":
                    AtencionAlCliente();
                    break;
                case "2":
                    ColaReparto();
                    break;
                case "3":
                    Cartas();
                    break;
                case "0":
                    return true;

                default:
                    Console.WriteLine("Esta opció no existeix");
                    break;
            }

            return false;
        }

        public void AtencionAlCliente()
        {
            ColaCliente cola = new ColaCliente();

            // Ciclo para que el usuario ingrese nuevas solicitudes hasta que el usuario decida salir 
            bool salir = false;
            while (!salir)
            {
                // Solicitar al usuario el nombre del cliente y el tipo de solicitud
                Console.WriteLine("Ingrese el nombre del cliente:");
                string nombreCliente = Console.ReadLine();
                Console.WriteLine("Ingrese el tipo de solicitud:");
                string tipoSolicitud = Console.ReadLine();

                // Agregar la solicitud a la cola
                cola.AgregarSolicitud(nombreCliente, tipoSolicitud);

                // Mostrar la siguiente solicitud en la cola
                cola.MostrarSiguienteSolicitud();

                
                Console.WriteLine("¿Ha atendido la solicitud mostrada? (S/N)");
                string respuesta = Console.ReadLine();
                if (respuesta.ToUpper() == "S")
                {
                    cola.EliminarSolicitudAtendida();
                }

                // Muestra la cantidad de solicitudes pendientes en la cola
                cola.SolicitudesPendientes();

                // Pedir al usuario si quiere salir del programa
                Console.WriteLine("¿Desea salir del programa? (S/N)");
                respuesta = Console.ReadLine();
                if (respuesta.ToUpper() == "S")
                {
                    salir = true;
                }
            }
        }

        
        public void ColaReparto()
        {
            ColaReparto colaReparto = new ColaReparto();
            Console.WriteLine("Bienvenido a Just Feed!");

            // Ciclo para que el usuario ingrese nuevas pedidos hasta que el usuario decida salir 

            while (true)
            {
                Console.WriteLine("¿Quieres agregar un pedido? (S/N)");
                string respuesta = Console.ReadLine().ToLower();
                
                if (respuesta == "n")
                {
                    break;
                }
                // Opcion para que el usuario pueda introducir sus datos
                else if (respuesta == "s")
                {
                    Console.WriteLine("Nombre del cliente:");
                    string nombreCliente = Console.ReadLine();

                    Console.WriteLine("Dirección de entrega:");
                    string direccionEntrega = Console.ReadLine();

                    Console.WriteLine("Platos (separados por coma):");
                    List<string> platos = new List<string>(Console.ReadLine().Split(','));

                    colaReparto.AgregarPedido(nombreCliente, direccionEntrega, platos);
                }
                else
                {
                    Console.WriteLine("Respuesta no válida, intenta de nuevo.");
                }
            }

            while (colaReparto.Count > 0)
            {
                colaReparto.MostrarProximoPedido();
                Console.WriteLine("¿Este pedido ya fue entregado? (S/N)");
                string respuesta = Console.ReadLine().ToLower();

                if (respuesta == "s")
                {
                    colaReparto.EliminarProximoPedido();
                }
                else if (respuesta == "n")
                {
                    Console.WriteLine("Siguiente pedido en la cola.");
                }
                else
                {
                    Console.WriteLine("Respuesta no válida, intenta de nuevo.");
                }
            }

            Console.WriteLine("No hay más pedidos en la cola. Gracias por usar Just Feed!");
        }
        public void Cartas()
        {
            MazoCartas mazo = new MazoCartas();

            // Añadir cartas 
            Carta carta1 = new Carta("Dragon Blanco De ojos Azules", "Monstruo", 8, "Invoca un monstruo de nivel 4");
            Carta carta2 = new Carta("Exodia Necross", "Conjuros", 4, "Niega el efecto de una carta del oponente");
            Carta carta3 = new Carta("Mago Oscuro", "Lanzador de Conjuros", 7, "Hace algo con la magia");
            mazo.AgregarCarta(carta1);
            mazo.AgregarCarta(carta2);
            mazo.AgregarCarta(carta3);
            
          

            
            Console.WriteLine("El mazo contiene " + mazo.Count + " cartas");
            Console.WriteLine("La carta en la cima del mazo es " + mazo.Peek().nombre);

            
            ManoJugador mano = new ManoJugador();

            mano.JugarCarta(carta2);
            mano.DescartarCarta(carta3);

            // Robar carta y añadirla al mazo
            Carta cartaRobada = mazo.RobarCarta();
            Console.WriteLine(cartaRobada);
            if (cartaRobada != null)
            {
                mano.AgregarCarta(cartaRobada);
            }

            // Comprobamos que la carta se añadió correctamente a la mano del jugador
            Console.WriteLine("El jugador tiene " + mano.mano.Count + " cartas en su mano");
            Console.WriteLine("La carta en la mano del jugador es " + mano.mano[0].nombre);

            Console.ReadLine();
        }
    }
    
}
