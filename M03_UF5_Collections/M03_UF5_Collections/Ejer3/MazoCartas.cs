﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class MazoCartas : Stack<Carta>
    {
        // Agregar una carta al mazo
        public void AgregarCarta(Carta carta)
        {
            Push(carta);
        }

        // Permite robar unacarta del mazo en el caso que haya cartas en el mazo
        public Carta RobarCarta()
        {
            if (Count > 0)
            {
                return Pop();
            }
            else
            {
                Console.WriteLine("El mazo está vacío");
                return null;
            }
        }

        public bool EstaVacio()
        {
            return Count == 0;
        }
    }
}
