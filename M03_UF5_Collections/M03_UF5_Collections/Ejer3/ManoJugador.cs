﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class ManoJugador
    {
        public List<Carta> mano = new List<Carta>();

        // Programa que permite usar una carta
        public void JugarCarta(Carta carta)
        {
            mano.Remove(carta);
            Console.WriteLine("Jugando carta: " + carta.nombre + " - Efecto: " + carta.efecto);
        }

        // Programa que permite descartar una carta
        public void DescartarCarta(Carta carta)
        {
            mano.Remove(carta);
            Console.WriteLine("Descartando carta: " + carta.nombre);
        }

        // Programa que permite añadir una carta
        public void AgregarCarta(Carta carta)
        {
            mano.Add(carta);
            Console.WriteLine("Añadiendo carta: " + carta.nombre);
        }

        public bool EstaVacia()
        {
            return mano.Count == 0;
        }
    }
}
