﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03_UF5_Collections
{
    class Carta
    {
        public string nombre;
        public string tipo;
        public int nivel;
        public string efecto;

        //Constructor de la clase carta
        public Carta(string nombre, string tipo, int nivel, string efecto)
        {
            this.nombre = nombre;
            this.tipo = tipo;
            this.nivel = nivel;
            this.efecto = efecto;
        }
        public override string ToString()
        {
            return "Nombre: " + nombre + " ,Tipo: " + tipo + " ,Nivel " + nivel + " , Efecto "+ efecto;
        }
    }
}
